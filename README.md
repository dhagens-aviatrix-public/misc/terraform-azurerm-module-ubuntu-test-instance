# Module AWS Ubuntu Test Instance

This module deploys a Ubuntu test instance in Azure

The following variables are required:

key | value
--- | ---
name | Name of the Ubuntu instance
subnet_id | Subnet ID in which the host will be launched
ssh_key | Key to insert into Ubuntu machine

The following variables are optional:

key | default | value
--- | --- | ---
ubuntu_image | 18.04-LTS | string to search for Ubuntu image
instance_username | ubuntu | Username to create on Azure instances.
instance_size | Standard_B1ls | Ubuntu instance size