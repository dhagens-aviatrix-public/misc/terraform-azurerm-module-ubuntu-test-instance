variable "name" {
    type = string
}

variable "ubuntu_image" {
    type = string
    default = "18.04-LTS"
}

/*
variable "pub_dns_suffix" {
    type = string
}

variable "priv_dns_suffix" {
    type = string
}
*/

variable "subnet_id" {
    type = string
}

variable "azure_region" {
    type = string
}

variable "resource_group_name" {
    type = string
}

variable "ssh_key" {
    type = string
}

variable "instance_size" {
    type = string
    default = "Standard_B1ls"
}

variable "instance_username" {
    type = string
    default = "ubuntu"
}

/*
variable "pub_ip" {
    type = bool
    default = false
}
*/