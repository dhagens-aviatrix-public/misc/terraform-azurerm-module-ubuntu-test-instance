resource "azurerm_network_interface" "default" {
  name                = "${var.name}-nic1"
  location            = var.azure_region
  resource_group_name = var.resource_group_name

  ip_configuration {
    name                          = "${var.name}-nic1"
    subnet_id                     = var.subnet_id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.default.id
  }
}

resource "azurerm_public_ip" "default" {
  name                = "${var.name}-ip"
  location            = var.azure_region
  resource_group_name = var.resource_group_name
  allocation_method   = "Dynamic"
}

resource "azurerm_virtual_machine" "default" {
  name                  = var.name
  location              = var.azure_region
  resource_group_name   = var.resource_group_name
  network_interface_ids = [azurerm_network_interface.default.id]
  vm_size               = var.instance_size

  delete_os_disk_on_termination    = true
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = var.ubuntu_image
    version   = "latest"
  }
  storage_os_disk {
    name              = "${var.name}-myosdisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = var.name
    admin_username = var.instance_username
  }
  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path     = "/home/${var.instance_username}/.ssh/authorized_keys"
      key_data = var.ssh_key
    }
  }
}