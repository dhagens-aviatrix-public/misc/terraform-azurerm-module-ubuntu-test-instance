output "azurerm_virtual_machine" {
    value = azurerm_virtual_machine.default
}

output "public_ip" {
    value = azurerm_public_ip.default.ip_address
}

output "private_ip" {
    value = azurerm_network_interface.default.private_ip_address
}